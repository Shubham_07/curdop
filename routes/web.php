<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ImageController;

use App\Http\Controllers\FileController;
use App\Http\Controllers\Admin\SliderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/create', function () {
//     return view('create');
// });
 
Route::resource('comment', 'App\Http\Controllers\CommentController');
Route::resource('comment', CommentController    ::class);
Route::get('/create', [ImageController::class, 'create']); 

  
// Route::get('/create', [ImageController::class, 'create']); 
// Route::post('/create', [ImageController::class, 'store']);

// Route::get('/create', [FileController::class, 'create']); 
// Route::post('/create', [FileController::class, 'store']);
// Route::get('file', [FileController::class, 'create']); 
// Route::post('file', [FileController::class, 'store']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


//CRUD FOR slider
Route::get('/admin/sliderlist', [SliderController::class, 'sliderlist']);
Route::get('/admin/sliderlist/create', [SliderController::class, 'create']);
Route::post('/admin/sliderlist/store', [SliderController::class, 'store']);
Route::get('/admin/deleteslider/{id}',  [SliderController::class, 'deleteslider']);
Route::get('/admin/editslider/{id}',  [SliderController::class, 'editslider']);
Route::post('/admin/updateslider/{id}',  [SliderController::class, 'updateslider']);





// Route::get('/admin/editdoctor/{d_id}', 'Admin\DoctorlistController@editdoctor');
// Route::post('/admin/updatedoctor/{d_id}', 'Admin\DoctorlistController@updatedoctor');


// Route::get('/user', [UserController::class, 'index']);

// Route::get('/admin/sliderlist', 'Admin\SliderController@sliderlist');
// Route::get('/admin/sliderlist/create', 'Admin\SliderController@create');
// Route::post('/admin/sliderlist/store', 'Admin\SliderController@store');
// Route::get('/admin/deleteslider/{id}', 'Admin\SliderController@deleteslider');