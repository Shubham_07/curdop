@extends('comment.layout')

@section('content')
    <div class="pull-left">
       <h2> comment section</h2>  
    </div>
    <div class="row">
        <div class="col-lg-12 margin-tb" >
            <div class="pull-right">
                <a class="btn btn-success" href="{{route('comment.create')}}" > new comment</a>
                 </div>
        </div>
    </div>

    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
                    <p>{{$message}}</p>
        </div>
    @endif
    

    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th> Email </th>
            <th>Comment</th>
             
            <th width="250px">Actions</th>
        </tr>
        @foreach ($comments as $comment)
            <tr>
                <td>{{++$i}} </td>
                <td>{{$comment->name}}</td>
                <td>{{$comment->email}}</td>
                <td> {{$comment->comment_line}}</td>
                <td>


                    <form action="{{route('comment.destroy', $comment->id) }}" method="POST">

                     <a class="btn btn-info "href="{{route('comment.show', $comment->id) }}" title="show">  Show </a>

                     <a class="btn btn-info "href="{{route('comment.edit', $comment->id) }}" title="show"> Edit  </a>
                 </a>

              @csrf
            @method('DELETE')
            <button type="submit " class="btn btn-danger"> Delete </button>
        </form>
                </td>
            </tr>

@endforeach
    </table>

<div> {{$comments->links()}}</div>
   
@endsection     