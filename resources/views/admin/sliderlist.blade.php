@extends('layouts.master')

@section('title', ' Manage Sliders')

@section('meta')

@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.13/r-2.1.1/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css"/>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.13/r-2.1.1/datatables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script>
        $(document).ready(function() {
        $('#table').DataTable( {
            // dom: 'Bfrtip',
            // buttons: [
            //     'copyHtml5',
            //     'excelHtml5',
            //     'csvHtml5',
            //     'pdfHtml5'
            // ]
        } );
    } );
</script>
@endsection

@section('content')


	<div class="container" style="margin-top:130px;">
					<div class="page-header row">
        <a class="btn btn-primary btn-lg " style="font-size:15px;background-color:#2ab27b;color:white;border-color:black;float:right;" href="{{ url('admin/') }}" >Back</a>
        <a class="btn btn-primary btn-lg " style="font-size:15px;color:white;border-color:black;float:right;margin-right: 10px;" href="{{ url('admin/sliderlist/create') }}" >New Image</a>
								<h3 class="col-md-8 col-sm-8 col-xs-8" style="text:centre">Manage Slider </h3>

		</div>
    </div>
    @if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<br><br>
			<strong>Success!</strong> {{ session('success') }}
		</div>
	@endif
	@if(session('danger'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> {{ session('danger') }}
		</div>
	@endif

	<div class="panel panel-default container"style="overflow:auto">
		<div class="panel-body row">
			<div class="col-md-12">
				<table id="table" class="table table-hover">
					<thead>
						<tr>
							<th>NO</th>
                            <th>Image</th>
							<th>Index</th>
							<th>Edit</th>
                            <th>Delete</th>
						</tr>
					</thead>

					<tbody>
						@if($sliders != null)
							@foreach($sliders as $slider)
								<tr>
									<td>{{++$i}}</td>
                                    <td>
                                        <center>
                                            <img class="slider" src="{{ asset('uploads/slider/'.$slider->img_path) }}" alt="Id Proof" style="width:80px;height:55px">
                                        </center>
                                    </td>
									<td>{{ $slider->index }}</td>
									<td><a href="{{url('/admin/editslider/'.$slider->id)}}" class='btn btn-success'>Edit</a></td>

                					<td><a href="{{url('/admin/deleteslider/'.$slider->id)}}" class="btn btn-danger">Delete</a></td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
