@extends('layouts.master')


@section('styles')
<link rel="stylesheet" href="{{asset('/css/jquery-ui.css')}}">
<link rel="stylesheet" href="/resources/demos/style.css">

<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-lite.min.css" rel="stylesheet">

<style>
    .text{
        margin-left:120px;
        margin-right:120px;
    }

    .btn
    {
        color:black
    }

</style>

@endsection

@section('content')
    <div class='container'>


{{-- 
@include('layouts.navbar') --}}

        <form 
        @isset($slider)
            action="{{url('/admin/updateslider/'.$slider->id)}}"
        @endisset
            action="{{url('/admin/sliderlist/store')}}"        
        id="Validate_form" method='POST' enctype='multipart/form-data'>
        {{csrf_field()}}
        <div class="text">
        <div>
            <div id="personaldetails">
                <h1>Slider Image :</h1>
                <div class="form-group">
                    <label for="img">Image: </label>
                    <input type="file" id="img_path" name="img_path" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="index">Index: </label>
                    <input type="number" id="index" name="index"class="form-control" >
                </div>
            </div>

            <button type="submit" class="btn btn-primary" name="submit">Save Data</button>

        </div>


        </form>
      <br/>
    </div>
    @endsection

@section('scripts')
<script src="{{ asset('/js/jquery-3.4.1.slim.min.js') }}" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="{{ asset('/js/summernote-lite.min.js') }}"></script>
<script>
    $(function(){


        $( "#datepicker" ).datepicker({
        changeMonth: true,
        changeYear: true
        });
    });

        $('#Validate_form').parsley();

</script>
@endsection

