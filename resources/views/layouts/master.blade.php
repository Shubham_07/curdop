<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/base.css')}}">
    <link rel="stylesheet" href="{{asset('/css/footer.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Lobster&display=swap" rel="stylesheet">


<style>
.card__title{
  text-transform: capitalize;
  font-size:19px;
}
body{
  background-color:#EBF5FB;
;
  color:# ;
/* background-image: url("../images/dark-wood.png"); */
/* This is mostly intended for prototyping; please download the pattern and re-host for production environments. Thank you! */
}
  .navbar-header{
    padding:0px;
  }
  .container-fluid{
    padding:0px;
  }
  #search{
    border:1px solid lightgray;
    border-radius:5px;
    width:70%;
  }
  #btn{
    background:#2ab27b;
    color:white;
    border:1px solid lightgray;
    border-radius:5px;
  }
  option {
   -webkit-border-radius:25px;
   font-family: 'Patua One', cursive;
   background:white;
   color:black;

}
  nav li:hover{
    background:#f27b48;
    color:white;
  }
  #top{
    background-color: #ffffff;
background-image: url("https://www.transparenttextures.com/patterns/bright-squares.png");
/* This is mostly intended for prototyping; please download the pattern and re-host for production environments. Thank you! */
  /* }
  #psearch{
  border-radius:5px;border:1px solid lightgray;
  padding-left:8px;
  box-shadow: 0 3px 6px 0 rgba(0,0,0,0.2);
  /* font-family: 'Lobster', cursive; */
} */
.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    /* left: -100%; */
    /* margin-left: 10px; */
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}

/* parsley */
input.parsley-success,
  select.parsley-success,
  textarea.parsley-success {
    color: #468847;
    background-color: #DFF0D8;
    border: 1px solid #D6E9C6;
  }

  input.parsley-error,
  select.parsley-error,
  textarea.parsley-error {
    color: #B94A48;
    background-color: #F2DEDE;
    border: 1px solid #EED3D7;
  }

  .parsley-errors-list {
    margin: 2px 0 3px;
    padding: 0;
    list-style-type: none;
    font-size: 0.9em;
    line-height: 0.9em;
    opacity: 0;

    transition: all .3s ease-in;
    -o-transition: all .3s ease-in;
    -moz-transition: all .3s ease-in;
    -webkit-transition: all .3s ease-in;
  }

  .parsley-errors-list.filled {
    opacity: 1;
  }

  .parsley-type, .parsley-required, .parsley-equalto, .parsley-pattern, .parsley-length{
   color:#cc0000;
  }
</style>
    @yield('styles')
  </head>
  <body>
    {{-- @include('partials.header') --}}

      @yield('content')

    {{-- @include('partials.footer') --}}


    <script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
    <script src="jquery-3.2.1.min.js"></script>
    <script src="{{ asset('/js/jquery-1.12.4.js') }}"></script>
    <script src="{{ asset('/js/jquery-ui.js') }}"></script>
{{-- parsley --}}
    <script src="{{ asset('/js/parsley.js') }}"></script>
{{-- parsley end --}}
<script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var dropdownContent = this.nextElementSibling;
      if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
      } else {
      dropdownContent.style.display = "block";
      }
      });
    }
</script>
      @yield('scripts')
  </body>

</html>





