<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sliderlist()
    {
        $sliders = Slider::all();

        return view('admin.sliderlist',['sliders'=>$sliders])->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.addslider');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider = new Slider;

        $public_path='/uploads/slider/';


        //image of img_path
        if($request->Hasfile('img_path'))
        {
            $file = $request->file('img_path');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/slider/',$filename);
            $slider->img_path = $filename;
        }else
        {

            $slider->img_path = '';
        }

        $slider->index = $request->input('index');

        $slider->save();
        return redirect('/admin/sliderlist')->with('success','Added Successuflly');
    }

  

    public function editslider($id)
    {
        $slider = Slider::find($id);
        return view('admin.addslider')->with('slider',$slider);
    }

    public function updateslider(Request $request,$id)
    {
            $slider = Slider::find($id);  
            $public_path='/uploads/slider/';                     
            $slider->index = $request->input('index');
            $slider_image_path=$slider->img_path;

            //image of index
            if($request->Hasfile('img_path'))
                {
                    if($slider_image_path)
                        unlink(public_path().$public_path.$slider_image_path);
                    $file = $request->file('img_path');
                    $extension = $file->getClientOriginalExtension();
                    $filename = time() . '.' . $extension;
                    $file->move('uploads/slider/',$filename);
                    $slider->img_path = $filename;

                }
   
            $slider->save();

            $slider = slider::all();
            return redirect('/admin/sliderlist')->with('success', 'slider has been successfully Updated');

        }
         
    

    public function deleteslider($id)
    {
        $slider = Slider::find($id);

        $public_path='/uploads/slider/';


        $img_path=$slider->img_path;


		if($img_path)
            unlink(public_path().$public_path.$img_path);

        $slider->delete();


        return redirect('/admin/sliderlist')->with('slider',$slider);
    }

}


