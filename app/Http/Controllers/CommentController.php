<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
  
    public function index()
    {
        $comments = Comment::latest() ->paginate(5);
        return view('comment.index', compact('comments'))
        ->with('i', (request()->input('page', 1)-1) * 5);

        
    }
    

   
    public function create()
    {
       return view('comment.create');
    }

   
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'comment_line' => 'required'
        ]);

        Comment::create($request->all());

        return redirect()->route('comment.index')
            ->with('success', 'Product created successfully.');

    }
 
    public function show(Comment $comment)
    {
        return view('comment.show', compact('comment'));
    }

   
    public function edit(Comment $comment)
    {
        return view('comment.edit', compact('comment'));
    }
 
    public function update(Request $request, Comment $comment)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'comment_line' => 'required'
        ]);
        $comment->update($request->all());

        return redirect()->route('comment.index')
            ->with('success', 'comment updated successfully');
    }    

    
    public function destroy(Comment $comment)
    {
        $comment->delete();

        return redirect()->route('comment.index')
            ->with('success', 'comment deleted successfully');
    }
}
